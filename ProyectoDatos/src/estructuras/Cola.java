package estructuras;

public class Cola<T> {

	private Nodo<T> first;
	private Nodo<T> last;
	private int size;

	public Cola() {
		this.first = null;
		this.last = null;
		this.size = 0;
	}

	public T dequeue() {
		T item = null;
		if (!isEmpty()) {
			item = first.getItem();
			first = first.getNext();
			size--;
		}

		if (isEmpty())
			last = null;
		return item;
	}

	public void enqueue(T item) {
		Nodo<T> oldLast = this.last;
		last = new Nodo<T>(item, null);
		if (isEmpty()) {
			first = last;
		} else {
			oldLast.setNext(last);
		}
		size++;
	}

	public boolean isEmpty() {
		return first == null;
	}

	public int getSize() {
		return size;
	}

	public static void main(String[] args){
		Cola<String> queue = new Cola<>();
		queue.enqueue("Hola");
		queue.enqueue("amigo");
		queue.enqueue("Chepe");
		
		String item = null;
		while( (item = queue.dequeue()) != null){
		
			System.out.println(item);
		}
		
	}
}
