package estructuras;

public class Nodo<T> {

	private T item;
	private Nodo<T> next;

	public Nodo() {

		this.item = null;
		this.next = null;
	}

	public Nodo(T item, Nodo<T> next) {
		super();
		this.item = item;
		this.next = next;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}

	public Nodo<T> getNext() {
		return next;
	}

	public void setNext(Nodo<T> next) {
		this.next = next;
	}

	public static void main(String[] args) {

		Nodo first = new Nodo();
		Nodo second = new Nodo();
		Nodo last = new Nodo();

		first.setItem("Hola");
		first.setNext(second);

		second.setItem("amigo");
		second.setNext(last);

		last.setItem("Chepe");

		for (Nodo x = first; x != null; x = x.getNext()) {
			System.out.println(x.getItem());
		}

	}

}
