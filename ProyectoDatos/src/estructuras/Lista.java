package estructuras;

public class Lista<T> {

	private Nodo<T> first;
	private int size;

	public Lista() {
		first = null;
		size = 0;

	}

	public Nodo<T> getFirst() {
		return first;
	}

	public int getSize() {
		return size;
	}

	public boolean isEmpty() {
		return size == 0;
	}

	public void add(T item) {
		Nodo<T> current = first;
		Nodo<T> newNode = new Nodo<T>();
		newNode.setItem(item);

		if (first == null) {
			first = newNode;
			size++;
			return;
		}

		while (current.getNext() != null) {
			current = current.getNext();
		}
		current.setNext(newNode);
		size++;
	}

	public void insertAtK(T item, int k) {
		Nodo<T> current = first;

		Nodo<T> newNode = new Nodo<T>();
		newNode.setItem(item);

		Nodo<T> temp = null;
		if (k == 0) {
			newNode.setNext(current);
			first = newNode;
			size++;
			return;
		}

		int i = 0;
		while (i < k && current != null) {
			temp = current;
			current = current.getNext();
			i++;
		}
		newNode.setNext(current);
		temp.setNext(newNode);
		size++;

	}

	public void deleteAtK(int k) {
		Nodo<T> previous = null;

		if (k == 0) {
			first = first.getNext();
			size--;
			return;
		}

		int i = 0;
		Nodo<T> current = first;

		while (i < k && current != null) {

			previous = current;
			current = current.getNext();

			i++;
		}
		previous.setNext(current.getNext());
		size--;

	}

	public static void main(String[] args){
		Lista<String> list = new Lista<>();
		list.add("Hola0");
		
		list.add("Hola1");
		list.add("Hola2");
		list.add("Hola3");
		list.add("Hola4");
		
		//list.insertAtK("Aja !!", 6);
		list.deleteAtK( 0);
		
		//Traverse the tree
		for(Nodo x = list.getFirst(); x != null; x=x.getNext()){
				System.out.println(x.getItem());
		}
	}
}
