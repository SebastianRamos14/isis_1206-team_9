package estructuras;

public class Pila<T> {

	private Nodo<T> top;
	private int size;

	public Pila() {
		this.top = null;
		this.size = 0;
	}

	public T getItemAtTop() {
		if (isEmpty()) {
			return null;
		}
		return top.getItem();
	}

	public boolean isEmpty() {
		return top == null;
	}

	public int getSize() {
		return size;
	}

	public void push(T item) {
		top = new Nodo<T>(item, top);
		size++;
	}

	public T pop() {
		T item = null;
		if (top != null) {
			item = top.getItem();
			top = top.getNext();
			size--;
		}
		return item;
	}

	public static void main(String[ ] args){
		Pila<String> stack = new Pila<>();
		
		stack.push("Hola");
		stack.push("amigo");
		stack.push("chepe");
		
		String item = null;
		while( (item = stack.pop()) != null){
		
			System.out.println(item);
		}
	}
}
