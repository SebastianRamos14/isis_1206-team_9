package parteC;



public class Carro {

	/**
	 * Matricula del vehiculo
	 */
	private String matricula;
	/**
	 * nombre del due�o del vehiculo
	 */
	private String nombreConductor;

	/**
	 * Crea el registro de un nuevo vehiculo al llegar al parqueadero
	 * 
	 * @param pColor
	 *            color del vehiculo
	 * @param pMatricula
	 *            matricula del vehiculo
	 * @param pNombreConductor
	 *            nombre de quien conduce el vehiculos
	 */

	public Carro(String pMatricula, String pNombreConductor) {

		matricula = pMatricula;
		nombreConductor = pNombreConductor;

	}

	/**
	 * Da la matricula del vehiculo
	 * 
	 * @return la matricula del vehiculo
	 */
	public String darMatricula() {
		return matricula;
	}

	/**
	 * Da el nombre de quien conduce el vehiculo
	 * 
	 * @return el nombre de quien conduce el vehiculo
	 */
	public String darNombreConductor() {
		return nombreConductor;
	}

}
