package parteC;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import estructuras.*;

public class Central<T> {
	/**
	 * Cola de carros en espera para ser estacionados
	 */
	Cola<T> colaEspera;

	/**
	 * Numero de parqueaderos en la central
	 */
	public final static int PARQUEADEROS = 5;
	/**
	 * Tama�o de los parqueaderos
	 */
	public final static int TAMANHO = 4;
	/**
	 * Pilas de carros parqueaderos 
	 */
	ArrayList<Pila<T>> pilasCarro;

	/**
	 * Pila de carros parqueadero temporal: Aca se estacionan los carros
	 * temporalmente cuando se quiere sacar un carro de un parqueadero y no es
	 * posible sacarlo con un solo movimiento
	 */
	Pila<T> pilaTemporal;

	/**
	 * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento
	 * temporal y la cola de carros que esperan para ser estacionados.
	 */
	public Central() {
		colaEspera = new Cola<T>();
		pilasCarro = new ArrayList<Pila<T>>();
		for (int i = 0; i < PARQUEADEROS; i++) {
			Pila<T> pilaNueva = new Pila<T>();
			pilasCarro.add(pilaNueva);
		}
		pilaTemporal = new Pila<T>();
	}

	public ArrayList<Pila<T>> darPilasParqueado() {
		return pilasCarro;
	}

	public Pila<T> darPilaTemporal() {
		return pilaTemporal;
	}

	public Cola<T> darColaEspera() {
		return colaEspera;
	}

	/**
	 * Registra un cliente que quiere ingresar al parqueadero y el vehiculo
	 * ingresa a la cola de carros pendientes por parquear
	 * 
	 * @param pColor
	 *            color del vehiculo
	 * @param pMatricula
	 *            matricula del vehiculo
	 * @param pNombreConductor
	 *            nombre de quien conduce el vehiculo
	 */
	public void registrarCliente(String pMatricula,
			String pNombreConductor) {
		Carro carroNuevo = new Carro( pMatricula, pNombreConductor);
		colaEspera.enqueue((T) carroNuevo);
	}

	/**
	 * Parquea el siguiente carro en la cola de carros por parquear
	 * 
	 * @return matricula del vehiculo parqueado y ubicaci�n
	 * @throws Exception
	 */
	public String parquearCarroEnCola() throws Exception {
		if (!colaEspera.isEmpty()) {
			Carro carroParquear = (Carro) colaEspera.dequeue();
			String ubicacion = parquearCarro(carroParquear);
			return "Se ha guardado el carro " + carroParquear.darMatricula()
					+ " en la ubicacion " + ubicacion;
		} else {
			throw new Exception("No hay carros para parquear");
		}
	}



	/**
	 * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el
	 * carro
	 * 
	 * @param aParquear
	 *            es el carro que se saca de la cola de carros que estan
	 *            esperando para ser parqueados
	 * @return El parqueadero en el que qued� el carro
	 * @throws Exception
	 */
	public String parquearCarro(Carro aParquear) throws Exception {
		boolean parqueado = false;
		int parqueadero = -1;
		for (int i = 0; i < PARQUEADEROS && !parqueado; i++) {
			Pila<T> parqueaderoAct = pilasCarro.get(i);
			if (parqueaderoAct.getSize() < TAMANHO) {
				parqueaderoAct.push((T) aParquear);
				parqueado = true;
				parqueadero = i + 1;
			}
		}
		if (!parqueado)
			throw new Exception("No hay parqueaderos disponibles");
		return "El carro quedo en el parqueadero " + parqueadero;
	}

	/**
	 * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
	 * 
	 * @param matricula
	 *            del vehiculo que se quiere sacar
	 * @return el carro buscado
	 * @throws Exception si no encuentra el carro
	 */
	public Carro sacarCarro(String matricula) throws Exception {
		Carro carroBuscado = null;
		if (sacarCarroP1(matricula) != null) {
			carroBuscado = sacarCarroP1(matricula);
		} else if (sacarCarroP2(matricula) != null) {
			carroBuscado = sacarCarroP2(matricula);
		} else if (sacarCarroP3(matricula) != null) {
			carroBuscado = sacarCarroP3(matricula);
		} else if (sacarCarroP4(matricula) != null) {
			carroBuscado = sacarCarroP4(matricula);
		} else if (sacarCarroP5(matricula) != null) {
			carroBuscado = sacarCarroP5(matricula);
		}
		if (carroBuscado == null)
			throw new Exception("No existe el carro con la matricula dada");
		return carroBuscado;
	}

	/**
	 * Saca un carro del parqueadero 1 dada su matricula
	 * 
	 * @param matricula
	 *            del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP1(String matricula) {
		Pila<T> parqueadero = pilasCarro.get(0);
		Carro buscado = null;
		while (!parqueadero.isEmpty() && buscado == null) {
			Carro carroAct = (Carro) parqueadero.pop();
			if (carroAct.darMatricula().equalsIgnoreCase(matricula))
				buscado = carroAct;
			else
				pilaTemporal.push((T) carroAct);
		}
		while (!pilaTemporal.isEmpty()) {
			Carro carroRec = (Carro) pilaTemporal.pop();
			parqueadero.push((T) carroRec);
		}
		return buscado;
	}

	/**
	 * Saca un carro del parqueadero 2 dada su matricula
	 * 
	 * @param matricula
	 *            del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP2(String matricula) {
		Pila<T> parqueadero = pilasCarro.get(1);
		Carro buscado = null;
		while (!parqueadero.isEmpty() && buscado == null) {
			Carro carroAct = (Carro) parqueadero.pop();
			if (carroAct.darMatricula().equalsIgnoreCase(matricula))
				buscado = carroAct;
			else
				pilaTemporal.push((T) carroAct);
		}
		while (!pilaTemporal.isEmpty()) {
			Carro carroRec = (Carro) pilaTemporal.pop();
			parqueadero.push((T) carroRec);
		}
		return buscado;
	}

	/**
	 * Saca un carro del parqueadero 3 dada su matricula
	 * 
	 * @param matricula
	 *            del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP3(String matricula) {
		Pila<T> parqueadero = pilasCarro.get(2);
		Carro buscado = null;
		while (!parqueadero.isEmpty() && buscado == null) {
			Carro carroAct = (Carro) parqueadero.pop();
			if (carroAct.darMatricula().equalsIgnoreCase(matricula))
				buscado = carroAct;
			else
				pilaTemporal.push((T) carroAct);
		}
		while (!pilaTemporal.isEmpty()) {
			Carro carroRec = (Carro) pilaTemporal.pop();
			parqueadero.push((T) carroRec);
		}
		return buscado;
	}

	/**
	 * Saca un carro del parqueadero 4 dada su matricula
	 * 
	 * @param matricula
	 *            del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP4(String matricula) {

		Pila<T> parqueadero = pilasCarro.get(3);
		Carro buscado = null;
		while (!parqueadero.isEmpty() && buscado == null) {
			Carro carroAct = (Carro) parqueadero.pop();
			if (carroAct.darMatricula().equalsIgnoreCase(matricula))
				buscado = carroAct;
			else
				pilaTemporal.push((T) carroAct);
		}
		while (!pilaTemporal.isEmpty()) {
			Carro carroRec = (Carro) pilaTemporal.pop();
			parqueadero.push((T) carroRec);
		}
		return buscado;
	}

	/**
	 * Saca un carro del parqueadero 5 dada su matricula
	 * 
	 * @param matricula
	 *            del vehiculo buscado
	 * @return carro buscado
	 */
	public Carro sacarCarroP5(String matricula) {
		Pila<T> parqueadero = pilasCarro.get(4);
		Carro buscado = null;
		while (!parqueadero.isEmpty() && buscado == null) {
			Carro carroAct = (Carro) parqueadero.pop();
			if (carroAct.darMatricula().equalsIgnoreCase(matricula))
				buscado = carroAct;
			else
				pilaTemporal.push((T) carroAct);
		}
		while (!pilaTemporal.isEmpty()) {
			Carro carroRec = (Carro) pilaTemporal.pop();
			parqueadero.push((T) carroRec);
		}
		return buscado;
	}

}
